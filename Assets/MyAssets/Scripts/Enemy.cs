﻿using UnityEngine;
using System.Collections;
using System;

public class Enemy : MonoBehaviour, Recyclable {
	public float speed;
	public float navUpdateInterval = 0.2f;

	private FlowField fieldRef;
	private Rigidbody body;
	private CapsuleCollider coll;
	private FlowField.Node currNode;
	private FlowField.Node lastValidNode;


	// Use this for initialization
	public void Start () {
		this.body = this.GetComponent<Rigidbody>();
		this.coll = this.GetComponent<CapsuleCollider>();

		Game.AddToList<Enemy>(this);

		Game.GetDelegate del = list => { if (list.Count > 0) return list[0]; return null; };
		fieldRef = Game.getFromList<FlowField>(del);

		StartCoroutine(this.UpdateNode());
	}
	
	// Update is called once per frame
	void Update () {
		if (fieldRef == null)
			return;

		if(this.currNode != null){
			Vector3 heading = this.currNode.direction;
			body.MovePosition(this.transform.position + heading * speed * Time.deltaTime);
		} else if(this.lastValidNode != null) {
			Vector3 heading = this.lastValidNode.direction;
			body.MovePosition(this.transform.position + heading * speed * Time.deltaTime);
		}
	}



	IEnumerator UpdateNode() {
		for (; ; ) {
			this.currNode = getNodeFromGraph();
			yield return new WaitForSeconds(this.navUpdateInterval);
		}
	}

	private FlowField.Node getNodeFromGraph(){
		FlowField.Node newNode = null;

		int[] currIndex = fieldRef.getIndexFromPoint(this.transform.position);

		if (fieldRef.isPointInsideGraph(currIndex)) {
			newNode = fieldRef.getNode(currIndex, this.transform.position.y - this.coll.bounds.extents.y);
			if (!newNode.valid) newNode = null;
		}

		if ((newNode == null || !newNode.valid) && (this.currNode != null && this.currNode.valid)) {
			this.lastValidNode = this.currNode;
		}

		return newNode;
	}

	void OnTriggerEnter(Collider coll) {
		if (coll.CompareTag("EnemyEnd")) {
			this.recycle();
			ObjectPool.destroy <Enemy> (this.gameObject);
			//Destroy(this.gameObject);
		}
	}


	public void recycle() {
		StopAllCoroutines();
		Game.RemoveFromList<Enemy>(this);
	}
}
