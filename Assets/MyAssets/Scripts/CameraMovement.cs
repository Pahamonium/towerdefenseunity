﻿using UnityEngine;
using System.Collections;

public class CameraMovement : MonoBehaviour {

	public float movementSpeed;
	public float rotateSpeed;
	public float zoomSpeed;
	public float angleDown;
	public Camera cameraObject;

	Vector3 forward;
	Vector3 left;
	Vector3 rotateLeft;

	// Use this for initialization
	void Start () {
		forward = new Vector3(0, 0, movementSpeed);
		left = new Vector3(-movementSpeed, 0, 0);
		rotateLeft = new Vector3(0,-rotateSpeed,0);

		cameraObject.transform.localRotation = Quaternion.Euler(new Vector3(angleDown, 0, 0));
	}
	
	// Update is called once per frame
	void Update () {
		float delta = Time.deltaTime;

		//Forward and backward.
		if (Input.GetKey(KeyCode.W))
			this.transform.Translate(forward * delta);
		else if (Input.GetKey(KeyCode.S))
			this.transform.Translate(-forward * delta);
		
		//Left and right
		if (Input.GetKey(KeyCode.A))
			this.transform.Translate(left * delta);
		else if (Input.GetKey(KeyCode.D))
			this.transform.Translate(-left * delta);
		
		//Rotate left and right
		if (Input.GetKey(KeyCode.Q))
		{
			this.transform.rotation = Quaternion.Euler(this.transform.rotation.eulerAngles + rotateLeft * delta);
		}
		else if (Input.GetKey(KeyCode.E))
		{
			this.transform.rotation = Quaternion.Euler(this.transform.rotation.eulerAngles - rotateLeft * delta);
		}

		//this.transform.Translate(Vector3.forward * delta);

		float scroll;
		if ((scroll = Input.GetAxis("Mouse ScrollWheel")) != 0)
		{
			Vector3 forw = this.cameraObject.transform.forward;

			Debug.Log("Scroll: " + scroll + " cam forward: " + forw + " zoom val: " + (forw * zoomSpeed * scroll * delta));
			this.transform.Translate(forw * zoomSpeed * scroll * delta);
		}
	}
}
