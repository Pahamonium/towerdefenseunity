﻿using UnityEngine;
using System.Collections;
using Pathfinding;

public class AStarAI : MonoBehaviour
{
    public Vector3 targetPosition;

    public Path path;

    public float speed = 300;

    public float nextWaypointDistance = 3;

    public bool dirty;

    private CharacterController controller;

    private int currentWaypoint = 0;

    private Seeker seeker;

    private Vector3 target;

    // Use this for initialization
    void Start() {
        seeker = GetComponent<Seeker>();
        controller = GetComponent<CharacterController>();

        seeker = this.GetComponent<Seeker>();
        Game.GetDelegate del;
        del = list => { return list[Random.Range(0, list.Count)]; };

        target = Game.getFromList<EndLocation>(del).transform.position;
        seeker.StartPath(transform.position, target, OnPathComplete);

        Game.AddToList<AStarAI>(this);
    }

    public void OnPathComplete(Path p) {
        Debug.Log("Yay, we got a path back. Did it have an error? " + p.error);
        if (!p.error) {
            this.path = p;
        }
    }

    public void FixedUpdate() {
        if (path == null) {
            //We have no path to move after yet
            return;
        }
        if (currentWaypoint >= path.vectorPath.Count) {
            Debug.Log("End Of Path Reached");
            return;
        }

        if (dirty) {
            seeker.StartPath(transform.position, target, OnPathComplete);
            dirty = false;
        }

        //Direction to the next waypoint
        Vector3 dir = (path.vectorPath[currentWaypoint] - transform.position).normalized;
        dir *= speed * Time.fixedDeltaTime;
        controller.SimpleMove(dir);
        //Check if we are close enough to the next waypoint
        //If we are, proceed to follow the next waypoint
        if (Vector3.Distance(transform.position, path.vectorPath[currentWaypoint]) < nextWaypointDistance) {
            currentWaypoint++;
            return;
        }
    }

    public void SetPath(Path path) {
        this.path = path;
    }

    void OnDestroy() {
        Game.RemoveFromList<AStarAI>(this);
    }

}
