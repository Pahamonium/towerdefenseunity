﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HexaSpawner : MonoBehaviour
{
    public GameObject hexagonToSpawn;
    public Transform HexaParent;
    public string TagToCheckAgainst;
    public LayerMask layerMaskForRaycast;

    private Bounds hexaBounds;
    private GameObject editorHex;
    private bool calledFromEditor = false;

    // Use this for initialization
    void Start() {
        MeshFilter meshFilter = (MeshFilter)hexagonToSpawn.GetComponent<MeshFilter>();
        this.hexaBounds = meshFilter.sharedMesh.bounds;

        this.enabled = false;

        this.layerMaskForRaycast = 1 << 2;
        this.layerMaskForRaycast = ~layerMaskForRaycast;

        if (this.getColliding(this.TagToCheckAgainst).Length > 0)
            StartCoroutine(spawnHexas());
    }

    // Update is called once per frame
    void Update() {

    }

    IEnumerator spawnHexas() {
        GameObject hexTmp;
        Bounds hexaBoundsTmp;
        Vector3 hexaSizeTmp;

        //Assign variables based on if it's being called from the editor or not.
        if (this.calledFromEditor) {
            hexTmp = this.editorHex;
            hexaBoundsTmp = (this.editorHex.GetComponent("MeshFilter") as MeshFilter).sharedMesh.bounds;
            hexaSizeTmp = hexaBoundsTmp.size;
        } else {
            hexTmp = this.hexagonToSpawn;
            hexaBoundsTmp = this.hexaBounds;
            hexaSizeTmp = this.hexaBounds.size;
        }

        //We'll use the spawner bounds to spawn the hexagons. The hexagons will only attempt to spawn on ground objects
        //that overlap the spawner and will spawn within the bounds of the box collider.
        Vector3 spawnerOrigin = this.transform.position;
        Vector3 spawnerScaledSize = this.transform.localScale;

        //The scaled size of the hex.
        Vector3 HexaScaledSize = Vector3.Scale(hexaSizeTmp, hexTmp.transform.localScale);

        //Cols are Z, rows are X. This is how many cols and rows are possible.
        int cols = (int)((spawnerScaledSize.z) / (HexaScaledSize.z));
        int rows = (int)((spawnerScaledSize.x) / (HexaScaledSize.x));

        //The spacing between each in cols and rows. For now, it's simply the lenght and width of the hexes.
        float spacingCols = HexaScaledSize.z;
        float spacingRows = HexaScaledSize.x;

        //This is the left and top point to start spawning from. This is essentially the
        //spawner origin - the spawner half size + the hex half size
        float groundLeftPoint = spawnerOrigin.z - spawnerScaledSize.z / 2 + HexaScaledSize.z / 2;
        float groundTopPoint = spawnerOrigin.x - spawnerScaledSize.x / 2 + HexaScaledSize.x / 2;

        //Some variables that may be edited during the loop.
        float oddRowOffset = 0;
        Vector3 rotation = new Vector3(0, 90, 0);

        //For each row...
        for (int row = 0; row < rows; row++) {
            //If it's an odd row, offset the objects by half of the object's width. Hexagonal placement! yeah!
            if (row % 2 == 1) oddRowOffset = HexaScaledSize.z / 2;
            else oddRowOffset = 0;

            //For each col...
            for (int col = 0; col < cols; col++) {
                //Calculate the X,Y,Z coords.
                float x = groundTopPoint + row * spacingRows;
                float y = this.transform.position.y + this.transform.localScale.y / 2;
                float z = groundLeftPoint + col * spacingCols + oddRowOffset;

                //Make an initial spawn location and a location to move to when the checks succeed
                Vector3 spawnLoc = new Vector3(x, y + hexaSizeTmp.y, z);

                //Create the hexagon in the air a bit.
                GameObject hexagon = Instantiate(hexTmp, spawnLoc, Quaternion.Euler(rotation)) as GameObject;

                //We need to create a list of vector3s
                List<Vector3> vectorList = new List<Vector3>();

                //Cahce the max and min of the hexagon bounds
                Vector3 max = hexagon.transform.position + hexaBoundsTmp.max;
                Vector3 min = hexagon.transform.position + hexaBoundsTmp.min;

                //Get the 4 corners of the bounding box, all at the bottom of the mesh.
                vectorList.Add(new Vector3(max.x, min.y, max.z));
                vectorList.Add(new Vector3(max.x, min.y, min.z));
                vectorList.Add(new Vector3(min.x, min.y, max.z));
                vectorList.Add(new Vector3(min.x, min.y, min.z));

                int counter = 0; //Counts the number of successful hits
                Vector3 pos = Vector3.zero;

                //Loop over the 4 corners doing checks down to the ground. If all 4 checks hit, we are good!
                foreach (Vector3 vector in vectorList) {
                    RaycastHit hit;
                    if (Physics.Raycast(vector, -Vector3.up, out hit, hexaSizeTmp.y * 2, layerMaskForRaycast.value)) {
                        if (hit.collider.gameObject.CompareTag(TagToCheckAgainst)) {
                            pos = hit.collider.transform.position;
                            counter++;
                        } else
                            break;
                    }
                }

                //If the counter was 4 (or more for some odd reason), our tests passed, move it to the right position!
                if (counter >= 4) {
                    //yield return null;
                    hexagon.transform.position = new Vector3(x, pos.y, z);
                    hexagon.transform.parent = HexaParent;
                } else
                    if (calledFromEditor) DestroyImmediate(hexagon);
                    else Destroy(hexagon);

                //yield return null;
            }
        }

        //If called from the editor, destroy the helper hex. Otherwise, destroy this gameobject.
        if (this.calledFromEditor) {
            DestroyImmediate(this.editorHex);
            this.calledFromEditor = false;
        } else
            Destroy(this.gameObject);

        yield return null;
    }

    /// <summary>
    /// Simple helper function to set the origin and local scale of this object.
    /// </summary>
    /// <param name="origin">The Vector3 position for the origin to be moved to.</param>
    /// <param name="size">The Vector3 size that the local scaled should be.</param>
    private void setOriginAndScale(Vector3 origin, Vector3 size) {
        this.transform.position = origin;
        this.transform.localScale = new Vector3(size.x, size.y + 1, size.z);
    }

    /* ------------------------------------------------ GUI HELPERS -----------------------------------------------*/

    public void editorSpawnHexagons() {
        //Collider[] colliders = getColliding(this.TagToCheckAgainst);
        this.calledFromEditor = true;
        this.editorHex = Instantiate(hexagonToSpawn) as GameObject;

        StartCoroutine(spawnHexas());
    }

    public void destroyEditorHexas() {
        foreach(Collider coll in getColliding("TowerSpot")){
            DestroyImmediate(coll.gameObject);
        }
    }

    /// <summary>
    /// Gets all colliding objects and returns a list of objects that match the tag parameter.
    /// </summary>
    /// <param name="tag">The tag string for each object to be compared to.</param>
    /// <returns>An array of colliders that matched the tag.</returns>
    Collider[] getColliding(string tag) {
        Vector3 scale = this.transform.localScale / 2;
        float radius = Mathf.Max(new float[] { scale.x, scale.y, scale.z });
        Collider[] colls = Physics.OverlapSphere(this.transform.position, radius);
        List<Collider> collList = new List<Collider>();

        foreach (Collider col in colls)
            if (col.CompareTag(tag))
                collList.Add(col);

        return collList.ToArray();
    }

}

