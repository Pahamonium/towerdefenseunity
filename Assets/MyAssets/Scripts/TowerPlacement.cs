﻿using UnityEngine;
using System.Collections;

public class TowerPlacement : MonoBehaviour {

	public GameObject towerBase;
	public GameObject camerObject;
	public string TagToMatch;
	public LayerMask hitMask;

	public Material shadowMaterial;

	private GameObject towerShadow;
	private bool onValidGround = false;

	// Use this for initialization
	void Start () {
		this.towerShadow = Instantiate(towerBase, Vector3.zero, Quaternion.Euler(0,90,0)) as GameObject;
		this.towerShadow.collider.enabled = false;
		this.towerShadow.renderer.material = shadowMaterial;
		this.shadowMaterial.color = Color.red;
	}
	
	// Update is called once per frame
	void Update () {
		Ray ray = camerObject.camera.ScreenPointToRay(Input.mousePosition);
		RaycastHit hit;

		//Perform a raycast...
		if (Physics.Raycast(ray, out hit, 100.0f, hitMask.value)) {
			//If it hit a gameobject with a matchin tag, call hitTowerSpot, otherwise call HitInvalid.
			if (hit.collider.gameObject.CompareTag(TagToMatch))
				this.hitTowerSpot(hit);
			else
				hitInvalid(hit);
		}

		checkClick(hit);
	}

	/// <summary>
	/// Called when a raycast hits a valid tower placement spot.
	/// </summary>
	/// <param name="hit"> The RaycastHit information.</param>
	void hitTowerSpot(RaycastHit hit) {
		if (!onValidGround) {
			onValidGround = true;
			this.shadowMaterial.color = Color.green;
		}
		this.towerShadow.transform.position = hit.collider.gameObject.transform.position;
	}

	/// <summary>
	/// Called when a raycast hits an invalid tower placement spot.
	/// </summary>
	/// <param name="hit"> The RaycastHit information.</param>
	void hitInvalid(RaycastHit hit) {
		if (onValidGround) {
			this.onValidGround = false;
			this.shadowMaterial.color = Color.red;
		}
		this.towerShadow.transform.position = hit.point;
	}

	/// <summary>
	/// Checks for mouse clicks. If clicked, places a tower if on a valid placement.
	/// </summary>
	/// <param name="hit">RaycastHit information to use for spawning the tower.</param>
	void checkClick(RaycastHit hit) {
		if (Input.GetMouseButtonDown(0)) {
			if (this.onValidGround) {
				Transform collTrans = hit.collider.gameObject.transform;
				GameObject obstacle = Instantiate(this.towerBase, collTrans.position, collTrans.rotation) as GameObject;
				AstarPath.active.UpdateGraphs(obstacle.collider.bounds);
			}
		}
	}
}
