﻿using UnityEngine;
using System.Collections;

public class Obstacle : MonoBehaviour {

	// Use this for initialization
	void Start () {
		Game.AddToList<Obstacle>(this);

		this.enabled = false;
	}
	
	// Update is called once per frame
	public void Update () {
		Debug.Log("UPDATING!");
	}

	public void FixedUpdate() {

	}
}
