﻿using UnityEngine;
using System.Collections;
using Pathfinding;

public class EnemySpawn : MonoBehaviour {

	public GameObject enemyToSpawn;
    public GameObject enemiesParent;
	public float initialTimeToSpawn;
	public float delayBetweenSpawns;
	public int amountToSpawn;
	private float endTimeToSpawn;
	private float nextTimeToSpawn;
	private bool spawning;
	private bool finished;
	private int amountSpawned;
	private Transform target;

	void Awake() {
		Game.AddToList<EnemySpawn>(this);
	}

	// Use this for initialization
	void Start () {
		this.endTimeToSpawn = Time.time + initialTimeToSpawn;
	}
	
	// Update is called once per frame
	void Update () {

		if (!spawning && Time.time > endTimeToSpawn)
			spawning = true;

		if (spawning && !finished) {
			if (amountSpawned >= amountToSpawn)
				finished = true;

			if (Time.time > nextTimeToSpawn) {
				nextTimeToSpawn = Time.time + delayBetweenSpawns;
                GameObject obj = ObjectPool.get<Enemy>(enemyToSpawn);
                obj.transform.position = this.transform.position;
                obj.transform.rotation = this.transform.rotation;
                obj.GetComponent<Enemy>().Start();
                obj.transform.parent = enemiesParent.transform;
				amountSpawned++;
			}
		}
	}
}
