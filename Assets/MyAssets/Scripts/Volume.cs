﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Volume : MonoBehaviour {

    public Color color = new Color(255, 255, 0, 1);
    public string[] tags;

    public void fitToAll() {
        Collider[] colliders = getColliding(this.tags);

        Vector3 maxBounds = Vector3.zero;
        Vector3 minBounds = Vector3.zero;
        Vector3 middlePos = Vector3.zero;
        bool atLeastOne = false;

        foreach (Collider ground in colliders) {

            //If it's zero, it hasn't been modified yet. Give it an initial value.
            if (maxBounds == Vector3.zero)
                maxBounds = ground.bounds.max;
            //Otherwise, compare it to the current ground.
            else
                maxBounds = Vector3.Max(maxBounds, ground.bounds.max);

            //If it's zero, it hasn't been modified yet. Give it an initial value.
            if (minBounds == Vector3.zero)
                minBounds = ground.bounds.min;
            //Otherwise, compare it to the current ground.
            else
                minBounds = Vector3.Min(minBounds, ground.bounds.min);

            //Add each ground position tot his vector
            middlePos += ground.gameObject.transform.position;

            atLeastOne = true;
        }

        if (!atLeastOne)
            return;

        //Divide by the number of ground objects to get the center.
        middlePos /= colliders.Length;

        //calculate the bounds
        Vector3 extents = (maxBounds - minBounds);

        middlePos = maxBounds - (maxBounds - minBounds) / 2;

        this.setOriginAndScale(middlePos, extents);
    }

    public void fitClosestOnly() {
        Collider[] colliders = getColliding(this.tags);
        Collider closest = null;
        float currDis = 99999999999999;

        //Get the closest overlapping ground to this collider.
        foreach (Collider coll in colliders) {
            if (closest == null) {
                closest = coll;
                continue;
            }

            float dst = Vector3.Distance(closest.transform.position, coll.transform.position);
            if (dst < currDis) {
                currDis = dst;
                closest = coll;
            }
        }

        if (!closest)
            return;

        //Set the position and bounds
        this.setOriginAndScale(closest.gameObject.transform.position, closest.bounds.size);
    }


    void OnDrawGizmos() {
        Gizmos.color = this.color;
        Gizmos.DrawWireCube(transform.position, this.transform.localScale);
    }

    /// <summary>
    /// Resets this object to a scale of 1,1,1
    /// </summary>
    public void resetToOne() {
        this.transform.localScale = new Vector3(1, 1, 1);
    }

    /// <summary>
    /// Gets all colliding objects and returns a list of objects that match the tag parameter.
    /// </summary>
    /// <param name="tag">The tag string for each object to be compared to.</param>
    /// <returns>An array of colliders that matched the tag.</returns>
    Collider[] getColliding(string[] tags) {
        Vector3 scale = this.transform.localScale / 2;
        float radius = Mathf.Max(new float[] { scale.x, scale.y, scale.z });
        Collider[] colls = Physics.OverlapSphere(this.transform.position, radius);
        List<Collider> collList = new List<Collider>();

        //Foreach collider, check against the array of string tags.
        foreach (Collider col in colls)
            foreach(string tagtmp in tags)
                if (col.CompareTag(tagtmp)) {
                    collList.Add(col);
                    break;
                }

        return collList.ToArray();
    }

    /// <summary>
    /// Simple helper function to set the origin and local scale of this object.
    /// </summary>
    /// <param name="origin">The Vector3 position for the origin to be moved to.</param>
    /// <param name="size">The Vector3 size that the local scaled should be.</param>
    private void setOriginAndScale(Vector3 origin, Vector3 size) {
        this.transform.position = origin;
        this.transform.localScale = new Vector3(size.x, size.y + 1, size.z);
    }
    
}
