﻿using UnityEngine;
using System.Collections;

public class NavAgent : MonoBehaviour {

	NavMeshAgent agent;

	// Use this for initialization
	void Start () {
		agent = this.GetComponent("NavMeshAgent") as NavMeshAgent;

		Game.GetDelegate del;
		del = list => { return list[Random.Range(0, list.Count)]; };

		agent.SetDestination(Game.getFromList<EndLocation>(del).transform.position);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
