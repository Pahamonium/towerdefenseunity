﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Threading;

public class FlowField : MonoBehaviour
{
    #region Public Inspector Variables
    public int numLayers = 1;
    public float squareSize = 1;
    public float minHeight = 5;
    public float maxSlope = 0.3f;
    public float maxStep = 0.3f;
    public float gapFromGround = 0.2f;
    public Transform testPoint;
    public GameObject end;
    public bool showImpassable;
    public bool showDis;
    public LayerMask walkableMask;
    public LayerMask ignoreMask;
    public LayerMask obstacleMask;
    #endregion

    public static Graph activeGraph;

    private Vector3 bottomLeft;
    private LayerMask ignoreMsk;
    private static FlowField singleton;

    void Awake() {
        Game.AddToList<FlowField>(this);
    }

    // Use this for initialization
    void Start() {
        FlowField.activeGraph = new Graph(this);
        FlowField.activeGraph.Init();        
    }

    #region Wrapper Functions
    /// <summary>
    /// Wrapper function to generate the graph.
    /// </summary>
    public void Generate() {
        FlowField.activeGraph = new Graph(this);

        FlowField.activeGraph.Generate();
    }

    /// <summary>
    /// Wrapper function to fill the graph.
    /// </summary>
    public void FillGraph() {
        FlowField.activeGraph.fillGraph();
    }

    /// <summary>
    /// Wrapper function to test a point on the graph.
    /// </summary>
    public void TestPoint() {
        FlowField.activeGraph.TestPoint();
    }

    /// <summary>
    /// Wrapper function to clear the graph
    /// </summary>
    public void ClearGraph() {
        FlowField.activeGraph.clear();
    }
    #endregion

    //Draw every node's gizmo
    void OnDrawGizmos() {
        if(FlowField.activeGraph != null)
            FlowField.activeGraph.OnDrawGizmos();
    }

    public class Graph
    {
        public bool valid = false;
        FlowField field;
        private Node[,][] graph;

        #region Constants
        private const float DIR_MARGIN = 0.05f;
        private const float MARGIN = 0.2f;
        private const int ROW = 0, COL = 1;
        private const int X = 0, Z = 1;
        private const int UP = 0, LEFT = 1, DOWN = 2, RIGHT = 3; //The way nodes are gathered as neighbors.
        private const int BR = 0, BL = 1, TL = 2, TR = 3; //The indices of a node. They are formed in this order.
        private const int MAXLOOPS = 20;
        #endregion

        public Graph(FlowField field) {
            this.field = field;
        }

        public void Init() {
            if (this.graph == null) {
                this.Generate();
                this.fillGraph();
            }
        }

        public void OnDrawGizmos() {
            if (graph != null && graph.Length > 0) {
                //For all columns
                for (int row = 0; row < graph.GetLength(0); row++) {
                    //for all rows
                    for (int col = 0; col < graph.GetLength(1); col++) {
                        if (graph[row, col] != null)
                            //for each node in this index
                            for (int n = 0; n < graph[row, col].Length; n++) {
                                //if it's not null, draw it.
                                if (graph[row, col][n] != null)
                                    graph[row, col][n].DrawGizmos(this.field.showImpassable, this.field.showDis);
                            }
                    }
                }
            }
        }


        #region Creating Nodes
        /// <summary>
        /// Generates the nodes for the graph.
        /// </summary>
        public void Generate() {
            //Set the graph to null and redeclare it's size.
            graph = null;
            graph = new Node[(int)(this.field.transform.localScale.x / this.field.squareSize), (int)(this.field.transform.localScale.z / this.field.squareSize)][];

            //Set a private mask to the opposite of the public ignore mask.
            this.field.ignoreMsk = ~this.field.ignoreMask;

            //Get the bottom left of this area and a compute a starting vector.
            this.field.bottomLeft = this.field.transform.position - this.field.transform.localScale / 2;
            Vector3 start = new Vector3(this.field.bottomLeft.x, this.field.transform.position.y, this.field.bottomLeft.z);

            //For every col and row, create nodes!
            for (int row = 0; row < graph.GetLength(0); row++) {
                for (int col = 0; col < graph.GetLength(1); col++) {
                    Vector3 pos = new Vector3(start.x + row * this.field.squareSize, start.y, start.z + col * this.field.squareSize);
                    graph[row, col] = CreateNode(pos, this.field.squareSize, new int[] { row, col });
                }
            }
        }

        /// <summary>
        /// Creates a node and returns it.
        /// </summary>
        /// <param name="bl"> The bottom left vector point of this square.</param>
        /// <param name="size">The size that the square should be.</param>
        /// <returns>A new Node to place in the grid.</returns>
        private Node[] CreateNode(Vector3 bl, float size, int[] index) {
            //Setup some needed temp variables
            Vector3[] points = new Vector3[4]; //Our 4 corners of the square
            Node[] nodes; //Our future node array

            //First, we get the four corners of the square.
            points[0] = new Vector3(bl.x, bl.y, bl.z); //Bottom left corner
            points[1] = new Vector3(bl.x, bl.y, bl.z + size); //Top left
            points[2] = new Vector3(bl.x + size, bl.y, bl.z + size); //Top right
            points[3] = new Vector3(bl.x + size, bl.y, bl.z); //Bottom right

            //Secondly, we need to figure out how many nodes there will be for this square. To find this, we use raycastall which goes through 
            //objects and returns an array of hits. We raycast at each corner of the square and record the highest number of VALID hits. The highest
            //value will be recorded and will set the amount for out nodes array.

            List<HitResult>[] hitResults;
            List<HitResult> objectsHit;
            int mostHitsIndes;

            //Third, we initialize the array with our nodes.
            nodes = new Node[this.getNumNodes(points, out hitResults, out objectsHit, out mostHitsIndes)];
            for (int i = 0; i < nodes.Length; i++) {
                nodes[i] = new Node(index);
            }

            //Fourth, we do a similar raycast as in the second part, but this time we will be recording the hit locations directly into the
            //nodes vertices.
            this.placeNodes(points, nodes, hitResults, objectsHit, mostHitsIndes);

            //Finally, we do a final raycast from all 4 points of the new node up to see if we hit anything. If we hit something within our 'minHeight' limit, the node
            //is not valid
            this.checkRequiredHeight(nodes);

            //Only do this check if still valid. Checks for the maximum slope between points.
            for (int n = 0; n < nodes.Length;n++ ) {
                Node node = nodes[n];
                node.finalize(); //Finalize the node. Might as well do it here.
                if (node.valid) {
                    node.valid = checkSlope(node.vertices); //Check if the node's slope is valid.
                }
            }

            //Since the node can't be guaranteed to be in the right order (lowest to highest) in the
            //y direction using its center, we need to sort the nodes from lowest to highest.
            Array.Sort<Node>(nodes, (n1, n2) => n1.center.y.CompareTo(n2.center.y));

            nodes = this.filterInvalidMinHeight(nodes);
            
            return nodes;
        }

        /// <summary>
        /// Determines the number of maximum nodes that will be created at this location.
        /// </summary>
        /// <param name="points">The four vertices that make up this node's (or nodes') location. This will be used to raycast.</param>
        /// <param name="hitResults">An array (of size 4) of Lists that hold vertex information. Each list represents a vertex hit.</param>
        /// <param name="mostHits"></param>
        /// <returns></returns>
        private int getNumNodes(Vector3[] points, out List<HitResult>[] hitResults, out List<HitResult> mostHits, out int mostHitsIndex) {
            int currNodeCounter = 0;
            int index = 0;
            int loopCounter = 0;
            mostHitsIndex = 0;

            //Init our out parameters
            hitResults = new List<HitResult>[4];
            mostHits = new List<HitResult>(10);

            //Initialize our hitResults array
            for (int i = 0; i < hitResults.Length; i++)
                hitResults[i] = new List<HitResult>(10);

            //Some tmp variables
            bool somethingHit = false;
            RaycastHit rayHit;
            int tmpHitCounter = 0;
            Vector3 lastHit = Vector3.zero;
            GameObject lastHitObj = null;

            //For each point, do repeated raycasts
            for (int i = 0; i < points.Length; i++) {
                Vector3 loc = points[i]; //Cache the vertex point
                lastHit = Vector3.zero; //Set the lastHit to zero
                somethingHit = true; //Set this to true initially.
                loopCounter = 0; //Reset the loopCounter;
                tmpHitCounter = 0; //Reset the tmpHitCounter;

                //Keep doing raycasts down until we don't hit anything.
                do {
                    //If the lastHit is not zero, set the loc.y to the lastHit.y plus a small offset.
                    if (lastHit != Vector3.zero) {
                        loc.y = (lastHit.y - 0.05f);
                    }

                    if (Physics.Raycast(loc, Vector3.down, out rayHit, 20, this.field.ignoreMsk.value)) {
                        if ((this.field.walkableMask.value & 1 << rayHit.collider.gameObject.layer) != 0) {
                            hitResults[i].Add(new HitResult(rayHit.collider.gameObject.GetHashCode(), rayHit.point)); //Assign a HitResult
                            tmpHitCounter++; //Increment the counter
                        }

                        lastHitObj = rayHit.collider.gameObject; //Set the lastHitObj
                        lastHit = rayHit.point; //Set the lastHit point.
                        loopCounter++; //Increment the loop counter

                        //If the loop counter exceeds a threshold, make it stop trying!
                        if (loopCounter > MAXLOOPS) {
                            Debug.LogError("Loop was stuck on point " + i + " lastHit was " + lastHit + " lastObj was " + lastHitObj.name);
                            break;
                        }

                    //If the raycast didn't hit, signal the loop to end.
                    } else
                        break;

                } while (somethingHit);
                

                //If the tmpCounter is larger than the current hitCounter, assign it! This will give us the 
                //number of maximum nodes and the index to the largest list at the end of the loop.
                if (tmpHitCounter > currNodeCounter) {
                    currNodeCounter = tmpHitCounter;
                    index = i;
                }

            }

            //Assign the hitResults at index to the mostHits out parameter;
            mostHits = hitResults[index];
            mostHitsIndex = index;

            return currNodeCounter;
        }

        /// <summary>
        /// Places a node at a certain location given the four corners of the node. This places the nodes
        /// in an order where the 0 index is the lowest node and the length-1 indes is the highest node.
        /// </summary>
        /// <param name="points">The four vertices that make up the node's location.</param>
        /// <param name="nodes">The list of uninitialized nodes that will be at this location. The length of this array is equal to the number
        /// of layers of nodes at this location.</param>
        /// <param name="hitResults">The HitResult info of each node's hit test from a previous calculation.</param>
        /// <param name="objectsHit">The List of HitResults from the Raycast through the most objects at a vertex.</param>
        private void placeNodes(Vector3[] points, Node[] nodes, List<HitResult>[] hitResults, List<HitResult> objectsHit, int mostHitsIndex) {
            //This specific counter it to make sure we have 4 hits. If we have fewer, that means one of our hits went off the map or something
            int currVertex = 0;

            //For each node at this location (or layer), place its vertices!
            for (int currNodeIndex = 0; currNodeIndex < nodes.Length; currNodeIndex++) {
                Node currNode = nodes[currNodeIndex]; //Cache the node.
                HitResult desiredObject = objectsHit[currNodeIndex];
                Vector3 tmpVertex = Vector3.zero;
                Vector3[] lastTwoVerts = new Vector3[2];

                bool finished = false;

                //Start and end values for the loop. What we want is to start at the index with the most hits. That way, the first vertex
                //will be the most valid vertex. This will help us later if we're trying to lay vertices that aren't good.
                int start = mostHitsIndex;

                int currListIndex = start;
                Vector3 desiredVert = Vector3.zero;

                //Loop from start to end. Because of the strange way the counters are used, we have to detect when the loop is finished
                //inside the loop. Essential, we want to be able to loop over an index as so: 3,0,1,2.
                while(!finished) {
                    List<HitResult> hitList = hitResults[currListIndex];
                    HitResult currHit = new HitResult();

                    //If the length of the list is greater than currNodeIndex, we can attempt to grab the HitResult
                    //for this layer.
                    if (hitList.Count > currNodeIndex)
                        //This attempts to grab the hit at this vertex index, but they rarely match up.
                        currHit = hitList[currNodeIndex];

                    if (currListIndex == start) {
                        desiredVert = currHit.point;
                    }

                    //If we are at index 1 or above, grab the last two vertices
                    if (currVertex > 0) {
                        int lastIndex = GeneralHelper.mod(currListIndex - 1, hitResults.Length);
                        lastTwoVerts[0] = currNode.vertices[lastIndex];
                        lastTwoVerts[1] = desiredVert;
                    }

                    //First, we want to see if this hit was on the desired object. 
                    //If not, we compare every hit at this vertex to the desired object and try to find the closest point.
                    if (currHit.point == Vector3.zero || currHit.objHash != desiredObject.objHash || (currVertex > 0 && !this.checkSlope(lastTwoVerts))) {
                        //We get some variables ready
                        float closestDist = Mathf.Infinity;
                        Vector3 closestPoint = Vector3.zero;

                        //Loop over each hit result at this index (hitCountercurrListIndex) and find the one closest to our desired object
                        foreach (HitResult tmpResult in hitResults[currListIndex]) {
                            //Calc distance in the y direction. If the distance is less than the current closest, assign it.
                            float tmpDis = Mathf.Abs(desiredVert.y - tmpResult.point.y);
                            if (tmpDis < closestDist) {
                                closestDist = tmpDis;
                                closestPoint = tmpResult.point;
                            }
                        }
                        //Set the vertex to the closest point
                        tmpVertex = closestPoint;

                        //If after the searching the vertex is still zero, it is off of any usable surfaces. 
                        //Set node to invalid and break.
                        if (tmpVertex == Vector3.zero) {
                            currNode.valid = false;
                            break;
                        }
                        //If the currHit is not null and the currHit object is our desired object, 
                        //simply set it to the currHit.point
                    } else 
                        tmpVertex = currHit.point;

                    //Set the vertex at this location.
                    currNode.vertices[currListIndex] = new Vector3(tmpVertex.x, tmpVertex.y + this.field.gapFromGround, tmpVertex.z);
                    currVertex++;

                    currListIndex = GeneralHelper.mod(currListIndex + 1, hitResults.Length);
                    if (currListIndex == start)
                        finished = true;
                }
                
                //Reset the counter
                currVertex = 0;
            }

        }

        /// <summary>
        /// Helper function that takes in an array of nodes that have been created, and tests for the required gap between the ground the node is on and any obstacles/objects
        /// above the node.
        /// </summary>
        /// <param name="nodes">An array of created Nodes to check if valid.</param>
        private void checkRequiredHeight(Node[] nodes) {
            RaycastHit hit;

            //Finally, we do a final raycast from all 4 points of the new node up to see if we hit anything. If we hit something within our 'minHeight' limit, the node
            //is not valid
            foreach (Node n in nodes) {
                for (int i = 0; i < n.vertices.Length; i++) {
                    Vector3 loc = n.vertices[i];
                    loc.y -= (this.field.gapFromGround);
                    //loc.y -= (this.field.gapFromGround + 0.05f);

                    //Perform a RaycastAll going through all objects
                    if(Physics.Raycast(loc, Vector3.up, out hit, this.field.minHeight * 2, this.field.ignoreMsk.value)){
                        if (hit.distance <= this.field.minHeight && hit.distance >= 0) {
                            n.valid = false;
                            break;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Checks between each vertex of the point array for an invalid slope value.
        /// </summary>
        /// <param name="points">The array of vertices to check between.</param>
        /// <returns>True if no invalid slope was detected, false otherwise.</returns>
        private bool checkSlope(Vector3[] points) {
            //Simply check between all vertices the differences in Y values.
            for (int i = 0; i < points.Length; i++) {
                if (Mathf.Abs(points[i].y - points[(i + 1) % points.Length].y) > this.field.maxSlope) {
                    return false;
                }
            }
            return true;
        }
        #endregion

        #region Building FlowMap
        /// <summary>
        /// Helper function that creates a thread and and executes it!
        /// </summary>
        public void fillGraph() {
            int[] index = this.getIndexFromPoint(this.field.end.transform.position); //Get the index for our end target.
            Node startNode = this.getNode(index, this.field.end.transform.position.y);

            FillGraphThread fillThread = new FillGraphThread(this, startNode);
            Thread thread = new Thread(new ThreadStart(fillThread.Fill));
            thread.Start();
        }

        /// <summary>
        /// Builds the flowmap of the graph. This include all distance and direction vector calculations.
        /// </summary>
        private void calcDistances(Node startNode) {
            if (graph == null) return;

            Queue<Node> nodesToVisit = new Queue<Node>(20); //Our queue for holding nodes that need to be visited

            Node currNode = startNode;

            currNode.target = true; //Set it to the target.
            int currValue = 1; //The value to add to each node.

            //Basic breadth-first search
            while (currNode != null) {
                Node[] neighbors = this.getImmediateValidNeighbors(currNode); //Get the nighbors of the current node.
                currNode.visited = true; //Set the node to visited so it won't be added again by another node.

                //For each node (layer) in that layerlist.
                foreach (Node neighborNode in neighbors) {
                    if (neighborNode == null)
                        continue;
                    //If it's not null and it's valid and not visited already, add it to the neighbors and trickle the value down.
                    if (!neighborNode.visited) {
                        if (!neighborNode.valid) //If the neighbor isn't valid, add double the additional distance.
                            neighborNode.distance = currNode.distance + currValue;
                        else { //Otherwise it's normal, proceed!
                            nodesToVisit.Enqueue(neighborNode);
                            neighborNode.visited = true;
                            neighborNode.distance = currNode.distance + currValue;
                        }
                    }
                }

                if (nodesToVisit.Count > 0)
                    currNode = nodesToVisit.Dequeue();
                else
                    currNode = null;
            }
        }

        /// <summary>
        /// This is called after creating the distances is done. This is a second breadth first search that calculates the heading
        /// vectors based on the tiles around.
        /// </summary>
        private void calcVectors(Node startNode) {
            Queue<Node> nodesToVisit = new Queue<Node>(20);

            Node currNode = startNode;

            while (currNode != null) {
                Node[] neighbors = this.getImmediateValidNeighbors(currNode);

                currNode.calculated = true;

                foreach (Node node in neighbors) {
                    if (node != null && node.valid && !node.calculated) {
                        node.calculated = true;
                        nodesToVisit.Enqueue(node);
                    }
                }

                //Calculate the directions if the currNode is not a target node.
                if (!currNode.target) {

                    float up = 0, left = 0, down = 0, right = 0; //Some tmp variables
                    up = left = down = right = currNode.distance; //Set them all to currNode's distance

                    //Left null but right is fine.
                    if (neighbors[LEFT] == null && neighbors[RIGHT] != null) {
                        left = neighbors[RIGHT].distance + 1;
                        right = neighbors[RIGHT].distance;
                    //Left fine but right null
                    } else if (neighbors[LEFT] != null && neighbors[RIGHT] == null) {
                        left = neighbors[LEFT].distance;
                        right = neighbors[LEFT].distance + 1;
                    //Left and right fine.
                    } else if (neighbors[LEFT] != null && neighbors[RIGHT] != null) {
                        left = neighbors[LEFT].distance;
                        right = neighbors[RIGHT].distance;
                    }

                    //Up null but down is fine.
                    if (neighbors[UP] == null && neighbors[DOWN] != null) {
                        up = neighbors[DOWN].distance + 1;
                        down = neighbors[DOWN].distance;
                        //Up fine but down null
                    } else if (neighbors[UP] != null && neighbors[DOWN] == null) {
                        up = neighbors[UP].distance;
                        down = neighbors[UP].distance + 1;
                        //Up and down fine.
                    } else if (neighbors[UP] != null && neighbors[DOWN] != null) {
                        up = neighbors[UP].distance;
                        down = neighbors[DOWN].distance;
                    }

                    //Calculate the heading
                    currNode.direction.z = left - right;
                    currNode.direction.x = down - up;
                    currNode.direction.Normalize();
                }

                //If there are still nodes left to dequeue, dequeue one! Otherwise, set currNode to null which will end
                //the search.
                if (nodesToVisit.Count > 0) currNode = nodesToVisit.Dequeue();
                else currNode = null;
            }

            for (int x = 0; x < graph.GetLength(0); x++) {
                for (int z = 0; z < graph.GetLength(1); z++) {
                    foreach (Node node in graph[x, z]) {
                        node.visited = false;
                        node.calculated = false;
                    }
                }
            }
        }

        /// <summary>
        /// Updates the graph by invalidating any nodes within the bounds passed in. The graph 
        /// will then recalculate the distances and vectors.
        /// </summary>
        /// <param name="bounds">The Bounds of an object/collider which will invalidate all
        /// nodes that it encompasses.</param>
        public void updateGraph(Bounds bounds) {
            Node[] nodes = this.getNodes(bounds.center, bounds.extents);
            foreach (Node node in nodes)
                node.valid = false;

            this.fillGraph();
        }
        #endregion

        #region Helper Functions
        private Vector3[] finishVertices(Vector3[] points) {
            Vector3 valid = Vector3.zero;
            Vector3[] newPoints = new Vector3[4];
            int dir = 0;

            for (int i = 0; i < points.Length; i++) {
                Vector3 point = points[i];
                dir = i;
                if (point != Vector3.zero) {
                    valid = point;
                    break;
                }
            }

            float SS = this.field.squareSize;

            for (int i = dir + 1; i != dir; i += (i + 1) % points.Length) {
                //int currDir = this.getDirOfVert()

                if (i == BR) {
                    newPoints[i] = new Vector3(valid.x - SS, valid.y, valid.z - SS);
                } else if (i == BL) {
                    newPoints[i] = new Vector3(valid.x - SS, valid.y, valid.z - SS);
                } else if (i == TL) {
                    newPoints[i] = new Vector3(valid.x - SS, valid.y, valid.z - SS);
                } else if (i == TR) {
                    newPoints[i] = new Vector3(valid.x - SS, valid.y, valid.z - SS);
                }

            }

            return new Vector3[] { };
        }

        /// <summary>
        /// Gets the direction of v1 from v2.
        /// </summary>
        /// <param name="v1">The vertex to get the direction of.</param>
        /// <param name="v2">The vertex to get the direction to v1</param>
        /// <returns>An integer (UP/DOWN/LEFT/RIGHT) indicating the direction.</returns>
        private int getDirOfVert(Vector3 v1, Vector3 v2) {
            float[] diffs = new float[2];

            diffs[0] = v2.x - v1.x; //For up or down.
            diffs[1] = v2.z - v1.z; //For left or right.

            //If "up or down" is larger than "left or right"...
            if (Mathf.Abs(diffs[0]) > Mathf.Abs(diffs[1])) {
                //If up/down is negative, it's down!
                if (diffs[0] < 0) return DOWN;
                else return UP;
                //If up/down is smaller, then!
            } else {
                //If left/right is negative, it's left!
                if (diffs[1] < 0) return LEFT;
                else return RIGHT;
            }
        }

        /// <summary>
        /// Converts a Vector3 point into an index which may or may not be valid on the flow field.
        /// </summary>
        /// <param name="point">The point to convert into an index</param>
        /// <returns>An index for the graph which may or may not be valid.</returns>
        public int[] getIndexFromPoint(Vector3 point) {
            Vector3 locInsideGraph = point - this.field.bottomLeft;
            locInsideGraph /= this.field.squareSize;

            return new int[] { (int)locInsideGraph.x, (int)locInsideGraph.z };
        }

        /// <summary>
        /// Checks if an index is inside (valid) the graph.
        /// </summary>
        /// <param name="index">The int array which contains the index for the graph. </param>
        /// <returns>True if the index is valid, false otherwise.</returns>
        public bool isPointInsideGraph(int[] index) {
            if (graph == null)
                return false;

            bool negative = index[0] < 0 || index[1] < 0;
            bool outOfBounds = index[0] >= graph.GetLength(0) || index[1] >= graph.GetLength(1);
            return !(negative || outOfBounds); //If negative or out of bounds, return false
        }

        /// <summary>
        /// Checks if a Vector3 location is inside the grid.
        /// </summary>
        /// <param name="location"> The Vector3 location to check.</param>
        /// <returns>True if the location is inside the graph, false otherwise.</returns>
        public bool isPointInsideGraph(Vector3 location) {
            return isPointInsideGraph(getIndexFromPoint(location));
        }

        /// <summary>
        /// Under construction
        /// </summary>
        /// <param name="location"></param>
        /// <param name="yLoc"></param>
        /// <returns></returns>
        public Node getClosestValidNode(Vector3 location, float yLoc) {
            if (graph == null)
                return null;

            int[] index = this.getIndexFromPoint(location);
            int offset = 1;
            Node closestNode = null;
            Node currClosest = null;
            float currClosestDist = Mathf.Infinity;

            while (closestNode == null) {
                //get the starting and ending index for X and Z.
                int startX = (index[0] - offset < 0) ? 0 : index[0] - offset;
                int startZ = (index[1] - offset < 0) ? 0 : index[1] - offset;
                int endX = (index[0] + offset < graph.GetLength(0)) ? graph.GetLength(0) - 1 : index[0] + offset;
                int endZ = (index[1] + offset > graph.GetLength(1)) ? graph.GetLength(1) - 1 : index[1] + offset;

                //Loop over the nodes from start to finish.
                for (int x = startX; x < endX; x++) {
                    for (int z = startZ; z < endZ; z++) {
                        //We are handling this like a breadth first search. Only take from the offset
                        if ((x != startX && x != endX) && (z != startZ && z != endZ))
                            continue;

                        //Get the closest node in the Y direction.
                        Node currClosestToY = this.getNode(new int[] { x, z }, yLoc);

                        if (!currClosestToY.valid)
                            continue;

                        //If currClosest is null or the new distance is less than the old, assign new values!
                        float tmpDis = (location - currClosestToY.center).sqrMagnitude;
                        if (currClosest == null || tmpDis < currClosestDist) {
                            currClosest = currClosestToY;
                            currClosestDist = tmpDis;
                        }
                    }
                }

                closestNode = currClosest;
            }

            return closestNode;
        }

        /// <summary>
        /// Uses a index array for accessing the X and Z components of a node, but requires a Y value to
        /// find the exact node that is closest to the yPos.
        /// </summary>
        /// <param name="index"></param>
        /// <param name="yPos"></param>
        /// <returns></returns>
        public Node getNode(int[] index, float yPos) {
            Node currClosest = null;
            float currDis = Mathf.Infinity; //Start with a huge number

            //Search through the nodes in this index and get the closest.
            foreach (Node node in graph[index[0], index[1]]) {
                float tmpDis = Mathf.Abs(node.center.y - yPos);
                if (currClosest == null || tmpDis < currDis) {
                    currDis = tmpDis;
                    currClosest = node;
                }
            }

            return currClosest;
        }

        /// <summary>
        /// Gets all nodes within an extent of a position.
        /// </summary>
        /// <param name="pos">The position (center) of the volume to get nodes.</param>
        /// <param name="extents">The extents in the X and Z directions to get nodes.</param>
        /// <returns></returns>
        public Node[] getNodes(Vector3 pos, Vector3 extents) {
            List<Node> nodes = new List<Node>(10);

            int[] startIndex = this.getIndexFromPoint(pos - extents);
            int[] endIndex = this.getIndexFromPoint(pos + extents);

            for (int x = startIndex[0]; x <= endIndex[0]; x++) { //col
                for (int z = startIndex[1]; z <= endIndex[1]; z++) { //row
                    foreach (Node node in graph[x, z])
                        nodes.Add(node);
                }
            }

            return nodes.ToArray();
        }

        public Node[] getPath(int[] index, float yPos){
            return this.getPath(this.getNode(index, yPos));
        }

        public Node[] getPath(Node node){
            List<Node> path = new List<Node>(20);

            Node currNode = node;
            Node[] neighbors;

            int counter=0;
            while (!currNode.target) {
                Node currBest = null;

                neighbors = this.getImmediateValidNeighbors8(currNode);
                foreach (Node neighbor in neighbors) {
                    if (neighbor == null)
                        continue;

                    if (currBest == null || neighbor.getScore() < currBest.getScore()) {
                        currBest = neighbor;
                    }
                }

                path.Add(currBest);
                currNode = currBest;

                counter++;
                if(counter > 1000){
                    Debug.LogError("Get path was taking too long.");
                    return new Node[]{};
                }
            }

            return path.ToArray();
        }

        

        /// <summary>
        /// Gets all neighbors of the node passed in, regardless if they are invalid or have too large of a step size.
        /// </summary>
        /// <param name="node">The node to get the neighbors of.</param>
        /// <returns>A 2D array of Nodes, where the first dimension is the direction of the neighbors and
        /// the second dimension is the nodes (or layer of nodes) in that direction.</returns>
        private Node[][] getAllNeighbors(Node node) {
            List<Node>[] neighbors = new List<Node>[4]; //To gather the initial neighbors
            List<Node>[] validNeighbors = new List<Node>[4]; //To hold the neighbors that are valid to the current node.

            int row = node.index[0]; //Get the X index of the node
            int col = node.index[1]; //Get the Z index of the node

            int neighborDir = 0;

            //Init both list arrays
            for (int i = 0; i < neighbors.Length; i++)
                neighbors[i] = new List<Node>(4);

            for (int i = 0; i < validNeighbors.Length; i++)
                validNeighbors[i] = new List<Node>(4);

            //Get the index of the up,left,down,right neighbors
            int[] up = new int[] { row + 1, col };
            int[] left = new int[] { row, col - 1 };
            int[] down = new int[] { row - 1, col };
            int[] right = new int[] { row, col + 1 };

            //Get the node in each direction if they are inside the graph.
            if (isPointInsideGraph(up))
                neighbors[UP] = new List<Node>(graph[up[0], up[1]]);
            if (isPointInsideGraph(left))
                neighbors[LEFT] = new List<Node>(graph[left[0], left[1]]);
            if (isPointInsideGraph(down))
                neighbors[DOWN] = new List<Node>(graph[down[0], down[1]]);
            if (isPointInsideGraph(right))
                neighbors[RIGHT] = new List<Node>(graph[right[0], right[1]]);

            //Get ALL nodes in each direction.
            //For each neighbor direction, get the (possible) list of neighbors.
            foreach (List<Node> neighborList in neighbors) {
                //For each node at that position (layers), check for slope and validness.
                foreach (Node nodeInLayer in neighborList) {
                    validNeighbors[neighborDir].Add(nodeInLayer);
                }

                neighborDir++;
            }

            ///Build the return list
            Node[][] returnNeighbors = new Node[4][];
            for (int i = 0; i < returnNeighbors.Length; i++) {
                returnNeighbors[i] = validNeighbors[i].ToArray();
            }

            return returnNeighbors;
        }

        /// <summary>
        /// Gets all VALID neighbors of the node passed in. Valid neighbors are nodes that are valid and have 
        /// an acceptable step level from the node passed in.
        /// </summary>
        /// <param name="node">The node to get the neighbors of.</param>
        /// <returns>A 2D array of Nodes, where the first dimension is the direction of the neighbors and
        /// the second dimension is the nodes (or layer of nodes) in that direction.</returns>
        private Node[][] getAllValidNeighbors(Node node) {
            List<Node>[] neighbors = new List<Node>[4]; //To gather the initial neighbors
            List<Node>[] validNeighbors = new List<Node>[4]; //To hold the neighbors that are valid to the current node.

            int row = node.index[0]; //Get the X index of the node
            int col = node.index[1]; //Get the Z index of the node

            int neighborDir = 0;

            //Init both list arrays
            for (int i = 0; i < neighbors.Length; i++)
                neighbors[i] = new List<Node>(4);

            for (int i = 0; i < validNeighbors.Length; i++)
                validNeighbors[i] = new List<Node>(4);

            //Get the index of the up,left,down,right neighbors
            int[] up = new int[] { row + 1, col };
            int[] left = new int[] { row, col - 1 };
            int[] down = new int[] { row - 1, col };
            int[] right = new int[] { row, col + 1 };


            if (isPointInsideGraph(up))
                neighbors[UP] = new List<Node>(graph[up[0], up[1]]);
            if (isPointInsideGraph(left))
                neighbors[LEFT] = new List<Node>(graph[left[0], left[1]]);
            if (isPointInsideGraph(down))
                neighbors[DOWN] = new List<Node>(graph[down[0], down[1]]);
            if (isPointInsideGraph(right))
                neighbors[RIGHT] = new List<Node>(graph[right[0], right[1]]);

            //For each neighbor direction, get the (possible) list of neighbors.
            foreach (List<Node> neighborList in neighbors) {
                //For each node at that position (layers), check for slope and validness.
                foreach (Node nodeInLayer in neighborList) {
                    if (this.validMaxStep(node, nodeInLayer) && nodeInLayer.valid) {
                        validNeighbors[neighborDir].Add(nodeInLayer);
                    }
                }

                neighborDir++;
            }

            ///Build the return list
            Node[][] returnNeighbors = new Node[4][];
            for (int i = 0; i < returnNeighbors.Length; i++) {
                returnNeighbors[i] = validNeighbors[i].ToArray();
            }

            return returnNeighbors;
        }

        /// <summary>
        /// Gets the immediate neighbors of a node. This means that only 1 node per direction will be returned. The neighbor
        /// will have to be valid and have an acceptable step from the node passed in.
        /// </summary>
        /// <param name="node">The node to get the neighbors of.</param>
        /// <returns>An array of nodes that are valid to the node passed in.</returns>
        private Node[] getImmediateValidNeighbors(Node node) {
            List<Node>[] neighbors = new List<Node>[4]; //To gather the initial neighbors
            List<Node>[] validNeighbors = new List<Node>[4]; //To hold the neighbors that are valid to the current node.

            int row = node.index[0]; //Get the X index of the node
            int col = node.index[1]; //Get the Z index of the node

            int neighborDir = 0;

            //Init both list arrays
            for (int i = 0; i < neighbors.Length; i++)
                neighbors[i] = new List<Node>(4);

            for (int i = 0; i < validNeighbors.Length; i++)
                validNeighbors[i] = new List<Node>(4);

            //Get the index of the up,left,down,right neighbors
            int[] up = new int[] { row + 1, col };
            int[] left = new int[] { row, col - 1 };
            int[] down = new int[] { row - 1, col };
            int[] right = new int[] { row, col + 1 };


            if (isPointInsideGraph(up))
                neighbors[UP] = new List<Node>(graph[up[0], up[1]]);
            if (isPointInsideGraph(left))
                neighbors[LEFT] = new List<Node>(graph[left[0], left[1]]);
            if (isPointInsideGraph(down))
                neighbors[DOWN] = new List<Node>(graph[down[0], down[1]]);
            if (isPointInsideGraph(right))
                neighbors[RIGHT] = new List<Node>(graph[right[0], right[1]]);

            //For each neighbor direction, get the (possible) list of neighbors.
            foreach (List<Node> neighborList in neighbors) {
                //For each node at that position (layers), check for slope and validness.
                foreach (Node otherNodeInLayer in neighborList) {
                    if (otherNodeInLayer.valid && this.validMaxStep(node, otherNodeInLayer)) {
                        validNeighbors[neighborDir].Add(otherNodeInLayer);
                        break;
                    }
                }

                //if the list we were just adding to is empty, add a null object.
                if (validNeighbors[neighborDir].Count == 0)
                    validNeighbors[neighborDir].Add(null);

                neighborDir++;
            }

            //We only want one node! Build the single array from the 2D list.
            ///Build the return list
            Node[] returnNeighbors = new Node[4];
            for (int i = 0; i < returnNeighbors.Length; i++) {
                returnNeighbors[i] = validNeighbors[i][0];
            }

            return returnNeighbors;
        }

        /// <summary>
        /// Gets the immediate neighbors of a node within 8 directions. This means that only 1 node per direction will be returned. The neighbor
        /// will have to be valid and have an acceptable step from the node passed in.
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        private Node[] getImmediateValidNeighbors8(Node node) {
            List<Node>[] neighbors = new List<Node>[8]; //To gather the initial neighbors
            List<Node>[] validNeighbors = new List<Node>[8]; //To hold the neighbors that are valid to the current node.

            int row = node.index[0]; //Get the X index of the node
            int col = node.index[1]; //Get the Z index of the node

            //Get the start and end indexes
            int[] start = new int[] { row - 1, col - 1 };
            int[] end = new int[] { row + 1, col + 1 };

            //Init both list arrays
            for (int i = 0; i < neighbors.Length; i++)
                neighbors[i] = new List<Node>(4);

            for (int i = 0; i < validNeighbors.Length; i++)
                validNeighbors[i] = new List<Node>(4);

            int neighborDir = 0;

            //Loop over all Nodes in this area.
            for (int rowx = start[0]; rowx <= end[0]; rowx++) {
                for (int colz = start[1]; colz <= end[1]; colz++) {

                    if (rowx != start[0] && rowx != end[0] && colz != start[1] && colz != end[1])
                        continue;

                    //If the point is not in the graph, continue
                    if (!isPointInsideGraph(new int[] { rowx, colz })) {
                        validNeighbors[neighborDir].Add(null);
                        continue;
                    }

                    //For each node at that position (layers), check for slope and validness.
                    foreach (Node otherNodeInLayer in graph[rowx,colz]) {
                        if (otherNodeInLayer.valid && this.validMaxStep(node, otherNodeInLayer)) {
                            validNeighbors[neighborDir].Add(otherNodeInLayer);
                            break;
                        }
                    }

                    //if the list we were just adding to is empty, add a null object.
                    if (validNeighbors[neighborDir].Count == 0)
                        validNeighbors[neighborDir].Add(null);

                    neighborDir++;
                }

            }

            //Get the first node out of the list and build the return list.
            Node[] returnNeighbors = new Node[8];
            for (int i = 0; i < returnNeighbors.Length; i++) {
                returnNeighbors[i] = validNeighbors[i][0];
            }

            return returnNeighbors;
        }

        /// <summary>
        /// Gets the direction (UP,LEFT,DOWN,RIGHT) that Node n1 is from Node n2
        /// </summary>
        /// <param name="n1">The first Node (from)</param>
        /// <param name="n2">The second Node (to)</param>
        /// <returns>An integer representing the direction from n1 to n2.</returns>
        private int getDirection(Node n1, Node n2) {
            float[] diffs = new float[2];

            //Subtract n1 from n2, this gives us negative for down/left and positive for up/right
            diffs[0] = n2.center.x - n1.center.x; //For up or down.
            diffs[1] = n2.center.z - n1.center.z; //For left or right.

            //If "up or down" is larger than "left or right"...
            if (Mathf.Abs(diffs[0]) > Mathf.Abs(diffs[1])) {
                //If up/down is negative, it's down!
                if (diffs[0] < 0) return DOWN;
                else return UP;
                //If up/down is smaller, then!
            } else {
                //If left/right is negative, it's left!
                if (diffs[1] < 0) return RIGHT;
                else return LEFT;
            }
        }

        /// <summary>
        /// A helper function that will return the closest vertex from a list to the vertex passed in.
        /// </summary>
        /// <param name="vert">The Vector3 vertex to get another Vector3 vertex close to.</param>
        /// <param name="otherVerts">A list of Vector3 vertices to find a close vertex in.</param>
        /// <returns>A Vector3 which is the closest vertext to 'vert'. </returns>
        private Vector3 getClosestVertex(Vector3 vert, Vector3[] otherVerts) {
            float currDis = 99999999999999999; //That's a big number
            Vector3 currClosest = Vector3.zero;

            //Loop over the Vectors3 and find the closest.
            foreach (Vector3 tmpVert in otherVerts) {
                float tmpDis = (tmpVert - vert).sqrMagnitude;
                if (tmpDis < currDis) {
                    currDis = tmpDis;
                    currClosest = tmpVert;
                }
            }

            return currClosest;
        }

        /// <summary>
        /// A helper function that determines if Node n2 steps too far up or down from Node n1.
        /// </summary>
        /// <param name="n1">The base Node to use for checking the step.</param>
        /// <param name="n2">The Node to check if the step is too high or low.</param>
        /// <returns>True if the step between n1 and n2 is okay (valid), false otherwise.</returns>
        private bool validMaxStep(Node n1, Node n2) {
            int dir = this.getDirection(n1, n2);

            Vector3[] verts = new Vector3[2];
            Vector3[] closestVerts = new Vector3[2];

            if (dir == UP) {
                verts[0] = n1.vertices[TL];
                verts[1] = n1.vertices[TR];
                closestVerts[0] = n2.vertices[BL];
                closestVerts[1] = n2.vertices[BR];

            } else if (dir == LEFT) {
                verts[0] = n1.vertices[BL];
                verts[1] = n1.vertices[TL];
                closestVerts[0] = n2.vertices[BR];
                closestVerts[1] = n2.vertices[TR];

            } else if (dir == DOWN) {
                verts[0] = n1.vertices[BL];
                verts[1] = n1.vertices[BR];
                closestVerts[0] = n2.vertices[TL];
                closestVerts[1] = n2.vertices[TR];

            } else if (dir == RIGHT) {
                verts[0] = n1.vertices[TR];
                verts[1] = n1.vertices[BR];
                closestVerts[0] = n2.vertices[TL];
                closestVerts[1] = n2.vertices[BL];
            }

            //If the step for both verts is under the threshold, return true!
            if (Mathf.Abs(verts[0].y - closestVerts[0].y) <= this.field.maxStep &&
                Mathf.Abs(verts[1].y - closestVerts[1].y) <= this.field.maxStep) {

                return true;
            }

            //If it failed, false
            return false;
        }

        /// <summary>
        /// Filters the list of nodes to exclude the nodes with not enough distance from the node above them.
        /// </summary>
        /// <param name="nodes">The array of Nodes to filter.</param>
        /// <returns>An array of valid Nodes.</returns>
        private Node[] filterInvalidMinHeight(Node[] nodes) {
            //We need to check that each node is within an acceptable distance of the next node.
            //From 0 to length-1 (one before the end), check if the next node's Y minus the ith node's Y is
            //less than or equal to the minHeight variable. If so, add it to the valid node list.
            if (nodes.Length > 1) {
                List<Node> validNodes = new List<Node>(10); //This is the list that will hold nodes that passed the test.
                bool noValid = true; //Records if there are no valid nodes above the current node.

                //For each node, we want to check the height to the next valid node above it.
                for (int i = 0; i < nodes.Length; i++) {

                    //If this node is not valid, continue to the next.
                    if (!nodes[i].valid)
                        continue;

                    //Check each node above this until we find a valid node.
                    for (int j = i + 1; j < nodes.Length; j++) {
                        //If not valid, continue!
                        if (!nodes[j].valid)
                            continue;

                        //If the next valid node above the one we are checking is under the min height, add it to
                        //the valid list and break.
                        if (nodes[j].center.y - nodes[i].center.y > this.field.minHeight)
                            validNodes.Add(nodes[i]);

                        //Set this flag to false
                        noValid = false;

                        break;
                    }

                    //If the loop ended without finding any valid nodes above it to check, it means the node is okay
                    //and valid.
                    if (noValid)
                        validNodes.Add(nodes[i]);

                    noValid = true;
                }

                validNodes.Add(nodes[nodes.Length - 1]); //Add the highest Node to the array always.
                return validNodes.ToArray();
            }

            return nodes;
        }
        #endregion

        #region Editor Functions
        public void TestPoint() {
            int[] index = getIndexFromPoint(this.field.testPoint.position);
            if (isPointInsideGraph(index))
                Debug.Log("Inside graph! WOOO, index: " + index[0] + " " + index[1]);
            else
                Debug.Log("Nope, not inside point...., index was: " + index[0] + " " + index[1]);
        }

        public void clear() {
            graph = null;
        }

        public int getGraphLengthX() {
            return (graph == null) ? 0 : graph.GetLength(0);
        }

        public int getGraphLengthZ() {
            return (graph == null) ? 0 : graph.GetLength(1);
        }

        #endregion

        #region Inner Classes

        private struct HitResult
        {
            public int objHash;
            public Vector3 point;
            public HitResult(int objHash, Vector3 point) {
                this.objHash = objHash;
                this.point = point;
            }
        }

        private class FillGraphThread
        {
            Graph graph;
            Node startNode;

            public FillGraphThread(Graph graph, Node startNode) {
                this.graph = graph;
                this.startNode = startNode;
                graph.valid = true;
            }

            public void Fill() {
                this.graph.calcDistances(this.startNode);
                this.graph.calcVectors(this.startNode);
            }
        }

        #endregion
    }

    /// <summary>
    /// A Class that represents a node of the graph.
    /// </summary>
    public class Node
    {
        public int[] index;
        public bool valid = true;
        public bool visited = false;
        public bool calculated = false;
        public bool target;
        public Vector3 direction;
        public float distance = 0;
        public float bonusScore = 0;
        public Vector3[] vertices = new Vector3[4];
        public Vector3 center;

        public Node(Vector3[] vertices, bool valid, int[] index) {
            this.vertices = vertices;
            this.valid = valid;
            this.center = calcCenter();
            this.index = index;
        }

        public Node(Vector3[] vertices, int[] index)
            : this(vertices, true, index) {

        }

        public Node(int[] index) {
            this.index = index;
        }

        /// <summary>
        /// Finalizes the node. This updates the center location for calculations and rendering debug.
        /// </summary>
        public void finalize() {
            this.center = calcCenter();
        }

        /// <summary>
        /// Drawing some debug info for the node.
        /// </summary>
        /// <param name="drawInvalid">If the node should draw if it's invalid.</param>
        /// <param name="showDis">If the node should show a distance line.</param>
        public void DrawGizmos(bool drawInvalid, bool showDis) {
            //If it's valid
            if (valid) {
                //If it's a target node, change it's color.
                if (target) Gizmos.color = Color.yellow;
                else Gizmos.color = Color.green;

                //Draw the square, one line to each vertex
                for (int vert = 0; vert < vertices.Length; vert++) {
                    Gizmos.DrawLine(vertices[vert], vertices[(vert + 1) % vertices.Length]);
                }

                //If it has been calculated, draw a direction line.
                if (this.direction != Vector3.zero) {
                    Gizmos.DrawLine(center, center + direction / 6);
                }
                //If it's not valid...
            } else {
                //if we're supposed to draw invalid nodes, draw it red!
                if (drawInvalid) {
                    Gizmos.color = Color.red;
                    for (int vert = 0; vert < vertices.Length; vert++) {
                        Gizmos.DrawLine(vertices[vert], vertices[(vert + 1) % vertices.Length]);
                    }
                }
            }

            //If we're supposed to show the distance, do so!
            if (showDis) {
                Gizmos.color = Color.yellow;
                Gizmos.DrawLine(this.center, this.center + Vector3.up * distance * 0.05f);
            }
        }

        /// <summary>
        /// Calculates the score of this node.
        /// </summary>
        /// <returns>A float which is the score for this node.</returns>
        public float getScore() {
            return distance + bonusScore;
        }

        /// <summary>
        /// Computes the center of this node.
        /// </summary>
        /// <returns>A Vector3 which is the center of this node.</returns>
        private Vector3 calcCenter() {
            Vector3 tmp = Vector3.zero;
            foreach (Vector3 point in vertices)
                tmp += point;
            tmp /= vertices.Length;

            return tmp;
        }

        public override string ToString() {
            return "index: " + index[0] + " " + index[1] + " vertices: " + vertices[0] + " " + vertices[1] + " " + vertices[2] + " " + vertices[3];
        }
    }
}