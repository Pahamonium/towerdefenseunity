﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class Game : MonoBehaviour{

	static Dictionary<Type, List<MonoBehaviour>> listDirectory = new Dictionary<Type, List<MonoBehaviour>>();

	public delegate MonoBehaviour GetDelegate(List<MonoBehaviour> list);
	public delegate void PerformDelegate(List<MonoBehaviour> list);

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	/// <summary>
	/// Adds an object to a list.
	/// </summary>
	/// <typeparam name="T">The class type of the object to add.</typeparam>
	/// <param name="Obj">The actual MonoBehaviour object to add.</param>
	public static void AddToList<T>(MonoBehaviour Obj) where T:class{
		if (!listDirectory.ContainsKey(typeof(T))) {
			List<MonoBehaviour> list = new List<MonoBehaviour>();
			listDirectory.Add(typeof(T), list);
		}

		listDirectory[typeof(T)].Add(Obj);
	}

	/// <summary>
	/// Removes a MonoBehaviour object from the list.
	/// </summary>
	/// <typeparam name="T">The class type of the MonoBehaviour</typeparam>
	/// <param name="Obj">The actual MonoBehaviour object to remove.</param>
	public static void RemoveFromList<T>(MonoBehaviour Obj) where T : class {
		if (!listDirectory.ContainsKey(typeof(T)))
			return;

		listDirectory[typeof(T)].Remove(Obj);
	}

	/// <summary>
	/// Takes in a delegate which will process the list and return a value.
	/// </summary>
	/// <typeparam name="T">The type of the class to find in the dictionary.</typeparam>
	/// <param name="del">The delegate to use for the list.</param>
	/// <returns>A Monobehavior of type T if found, otherwise null.</returns>
	public static T getFromList<T>(GetDelegate del) where T:class{
		if(!listDirectory.ContainsKey(typeof(T)))
			return null;

		return del(listDirectory[typeof(T)]) as T;
	}

	/// <summary>
	/// Performs an operation on the entire list.
	/// </summary>
	/// <typeparam name="T">The type of the class to find in the dictionary.</typeparam>
	/// <param name="del">The delegate to use on the list.</param>
	public static void performOnList<T>(PerformDelegate del) where T : class {
		if(!listDirectory.ContainsKey(typeof(T)))
			return;

		del(listDirectory[typeof(T)]);
	}
}
