﻿using UnityEngine;
using System.Collections.Generic;
using System;

public class ObjectPool : MonoBehaviour {

	static Dictionary<Type, List<GameObject>> pool = new Dictionary<Type, List<GameObject>>(5);

	// Use this for initialization
	void Start () {
		
	}

	public static void destroy<T>(GameObject obj) where T : class {
		if (!pool.ContainsKey(typeof(T)))
			pool.Add(typeof(T), new List<GameObject>(100));

		obj.SetActive(false);
		pool[typeof(T)].Add(obj);
	}

	public static GameObject get<T>(GameObject defaultObj) where T : class {
		if (!pool.ContainsKey(typeof(T)) || pool[typeof(T)].Count < 1) {
			return Instantiate(defaultObj, Vector3.zero, Quaternion.identity) as GameObject;
		}

		GameObject obj = pool[typeof(T)][pool[typeof(T)].Count - 1]; //Get the object at the last index;
		pool[typeof(T)].RemoveAt(pool[typeof(T)].Count - 1); //Remove the object at the end.
		obj.SetActive(true);
		return obj; //Return it!
	}
	
}
