﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(FlowField))]
public class FlowFieldEditor : Editor {

    public override void OnInspectorGUI() {
        base.OnInspectorGUI();

        FlowField script = (FlowField)target;

        script.minHeight = Mathf.Clamp(script.minHeight, 0.01f, (float)Mathf.Infinity);
        script.squareSize = Mathf.Clamp(script.squareSize, 0.2f, (float)Mathf.Infinity);
        EditorGUILayout.IntField("X length", script.getGraphLengthX());
        EditorGUILayout.IntField("Z length", script.getGraphLengthZ());
        

        if (GUILayout.Button("Generate"))
            script.Generate();

        if (GUILayout.Button("FillGraph"))
            script.fillGraph();

        if (GUILayout.Button("TestPoint"))
            script.TestPoint();

        if (GUILayout.Button("Clear Graph"))
            script.clear();

    }
}
