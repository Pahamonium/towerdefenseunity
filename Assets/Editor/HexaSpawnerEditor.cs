﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(HexaSpawner))]
public class HexaSpawnerEditor : Editor {

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        HexaSpawner script = (HexaSpawner)target;

        if (GUILayout.Button("Create Hexagons"))
            script.editorSpawnHexagons();

        if (GUILayout.Button("Destroy Overlapping Hexagons"))
            script.destroyEditorHexas();
    }

    
}
