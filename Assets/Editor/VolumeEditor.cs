﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(Volume))]
public class VolumeEditor : Editor
{
    string currTag = "Ground";

    public override void OnInspectorGUI() {
        base.OnInspectorGUI();

        Volume script = (Volume)target;

        if (GUILayout.Button("Fit To All"))
            script.fitToAll();

        if (GUILayout.Button("Fit To Closest"))
            script.fitClosestOnly();

        if (GUILayout.Button("Reset To 1"))
            script.resetToOne();

        currTag = EditorGUILayout.TagField(currTag);

    }


}
